# Projeto 1 - Motor de Fisica simples

O objetivo deste projeto é desenvolver um motor de fisica simples e posteriormente exibir graficamente usando OpenGL nossas simulações fisicas, que contém conjuntos massa-mola distintos.

Para realizar este projeto foram utilizados os arquivos disponibilizados por [Oxford](http://www.robots.ox.ac.uk/~victor/teaching/labs/b16/) que contém o projeto base e instruções para seu desenvolvimento.

	Configured with: --prefix=/Library/Developer/CommandLineTools/usr --with-gxx-include-dir=/usr/include/c++/4.2.1
	Apple LLVM version 9.0.0 (clang-900.0.37)
	Target: x86_64-apple-darwin16.7.0
	Thread model: posix
	InstalledDir: /Library/Developer/CommandLineTools/usr/bin


A compilação necessita de diversas flags, mas é facilitada por um makefile que já acompanha os arquivos base do projeto.

	[TODO]Inclua uma breve descrição do que cada arquivo contém ou faz.

### simulation.h

	Classe basica de simulação fisica, contém os métodos:
```c++
virtual void step(double dt);
//Atualiza os atributos fisicos do objeto mediante a passagem de tempo dt, acarreta leve imprecisão pois dt não é infinitesimal, visivel ao se passar um dt exageradamente grande.
virtual void display();
//Exibe graficamente o objeto em openGL.
```

### ball.cpp, ball.h

	Classe bola, herda de Simulation possui raio, posição e limites em X e Y.

```c++
double getX();
double getY();
void setX(double newX);
void setY(double newY);
void setPosition(double x, double y);
void step(double dt);
//Atualiza os atributos fisicos do objeto mediante a passagem de tempo dt, respeitando os limites em X e Y levando em consideração o raio r da bola.
void display();
//Exibe graficamente o objeto em openGL.
```

### test-ball.cpp

	Cria uma bola e executa 2500 iterações de 0.1 segundos da simulação fisica com uma bola, inicializada com atributos padrões.

### test-ball-graphics.cpp

	[TODO]

### springmass.h, springmass.cpp

```c++
[TODO]
void step(double dt);
//Atualiza os atributos fisicos do objeto mediante a passagem de tempo dt, respeitando os limites em X e Y levando em consideração o raio r da bola.
void display();
//Exibe graficamente o objeto em openGL.
```

### test-springmass.cpp

	[TODO]

### test-springmass-graphics.cpp

	[TODO]

### graphics.cpp, graphics.h

```c++
[TODO]
```



Como resultado da segunda questão o programa não existe exatamente um arquivo de saida, mas cada iteração do programa imprime no terminal a posição da bola dentro da nossa simulação fisica.

[TODO]--Inclua o diagrama das classes usadas na seção 3 do roteiro. (Seção 5?)

![Grafico Posição](https://gitlab.com/giraya.thiago/TP1projeto1/raw/master/images/test-ball-plot.png "Grafico Posição X vs Y")

Este grafico foi gerado usando R e um arquivo de saida "out.txt".

Para gerar "out.txt" foram usadas as seguintes linhas de comando:

![Out.txt](https://gitlab.com/giraya.thiago/TP1projeto1/raw/master/images/test-ball-outcode.png "pipe")

Para gerar o grafico após navegar para a pasta contendo out.txt foram usadas as seguintes linhas de comando:

![plot](https://gitlab.com/giraya.thiago/TP1projeto1/raw/master/images/test-ball-plotcode.png "R - plot(ball)")
