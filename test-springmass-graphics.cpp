/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{

  SpringMassDrawable()
  : figure("Spring Mass")
  {
    figure.addDrawable(this) ;
  }

  void draw(){
    for (int i  = 0; i < this.masses.size(); i++){
      Mass *m = &this.masses[i];
      figure.drawCircle(m->position.x, m->position.y, m->radius);
    }
    for (int i = 0; i < this.springs.size(); i++){
      Spring *s = &this.springs[i];
      Mass *m1 = s->getMass1();
      Mass *m2 = s->getMass2();
      figure.drawLine(m2->getPosition().x, m2->getPosition().y, m1->getPosition().x, m1->getPosition().y);
    }
  }
  void display(){
    figure.update();
  }

} ;



int main(int argc, char** argv)
{
  glutInit(&argc,argv);

  SpringMassDrawable springmass ;
  const double mass = 0.05 ;
  const double radius = 0.025 ;
  const double naturalLength = 1;
  const double stiffness = 0.1;
  const double damping = 0.1;

  Mass m1(Vector2(-.5,0), Vector2(), mass, radius) ;
  Mass m2(Vector2(+.5,0), Vector2(), mass, radius) ;

  /* INCOMPLETE: TYPE YOUR CODE HERE
     1. Adicione as duas massas instanciadas acima (2 linhas).
     2. Adicione uma mola com as duas massas (indexadas por 0 e 1),
        e com o parametro de comprimento em repouso inicializado acima.
	(1 linha).
   */

  springmass.addMass(m1);
  springmass.addMass(m2);
  springmass.addSpring(0,1,naturalLength, stiffness, damping);

  run(&springmass, 1/120.0) ;
}
