/** file: ball.cpp
 ** brief: Ball class - implementation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

#include <iostream>
// Radius 0.1, Position (0,0), Velocity(0.3, -0.1), Gravity (9.8), Mass(1), Inside a box centered at (0,0) with bounds at (-1,1) for X and Y
Ball::Ball()
: r(0.1), x(0), y(0), vx(0.3), vy(-0.1), g(9.8), m(1),
xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }

Ball::Ball(double x, double y){
  setPos(x,y);
}

void Ball::setX(double newX){
  x = newX;
}

void Ball::setY(double newY){
  y = newY;
}

void Ball::setPos(double x, double y){
  setX(x);
  setY(y);
}

double Ball::getY(){
  return y;
}

double Ball::getX(){
  return x;
}

void Ball::step(double dt)
{
  double xp = x + vx * dt ;
  double yp = y + vy * dt - 0.5 * g * dt * dt ;
  if (xmin + r <= xp && xp <= xmax - r) {
    x = xp ;
  } else {
    vx = -vx ;
  }
  if (ymin + r <= yp && yp <= ymax - r) {
    y = yp ;
    vy = vy - g * dt ;
  } else {
    vy = -vy ;
  }
}

void Ball::display()
{
  std::cout<<x<<" "<<y<<std::endl ;
}
